from uuid import uuid4

from django.db import models
from django.core import validators
from django.contrib.auth.models import AbstractUser
from apps.common.constants import UserType
from apps.common.models import AbstractBaseModel


# Create your models here.


class User(AbstractUser, AbstractBaseModel):
    number = models.CharField(
        max_length=30,
        verbose_name='Phone Number',
        # validators=
    )
    type = models.CharField(
        max_length=10,
        default=UserType.CONSUMER,
        choices=UserType.choices,
        verbose_name='User Type'
    )
    geo = models.CharField(
        max_length=100,
        blank=True,
        null=True,
        verbose_name='Geoglocation'
    )
    rating = models.PositiveIntegerField(
        default=0,
        blank=True,
        null=True,
        verbose_name='Rating'
    )

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def str(self):
        return f'{self.username} Phone:{self.number}'

    def clean(self):
        super().clean()
        if self.type == UserType.PRODUCER:
            if self.geo is None:
                raise validators.ValidationError(
                    'You must indicate your location'
                )
            if self.rating is None:
                raise validators.ValidationError(
                    'You must indicate your rating'
                )
