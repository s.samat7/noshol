from django.db import models
from django_editorjs import EditorJsField

from apps.users.models import User
from apps.common.models import AbstractBaseModel

class Category(AbstractBaseModel):
    name = models.CharField(max_length=120, verbose_name='Название')
    parent = models.ForeignKey(
        'self', on_delete=models.CASCADE,
        related_name='children',
        verbose_name='Родительская категория'
    )

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name

class Bitch(AbstractBaseModel):
    name = models.CharField(
        max_length=50,
        verbose_name='name'
    )
    description = EditorJsField()
    image = models.ImageField(upload_to='static/image/')
    price = models.DecimalField(
        max_digits=5,
        decimal_places=2
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='users',
        verbose_name='author'
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        related_name='categories',
        verbose_name='category'
    )

    class Meta:
        verbose_name = 'Bitch'
        verbose_name_plural = 'Bitches'

    def __str__(self):
        return self.name


class Image(models.Model):
    photo = models.FileField(upload_to='static/images/')
    bitch = models.ForeignKey(
        Bitch,
        on_delete=models.CASCADE,
        related_name='images',
        verbose_name='Image'
    )
    is_preview = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Image'
        verbose_name_plural = 'Images'

    def __str__(self):
        return self.photo